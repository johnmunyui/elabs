from django.apps import AppConfig


class ElabConfig(AppConfig):
    name = 'elab'
