from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Patient(models.Model):
    # (This is the patients' model class with basic information about them)

    INSURANCE_CHOICES = (
        ('TYPE 1', 'TYPE 1'),
        ('TYPE 2', 'TYPE 2'),
        ('TYPE 3', 'TYPE 3'),
        ('TYPE 4', 'TYPE 4'),
    )
    added_by = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=15, null=False)
    last_name = models.CharField(max_length=15, null=False)
    middle_name = models.CharField(max_length=15, blank=True)
    date_of_birth = models.DateField()
    email = models.EmailField(max_length=254)
    mobile_number = models.CharField(max_length=10, blank=False, null=False)
    id_number = models.CharField(max_length=10)
    passport_number = models.CharField(max_length=10)
    allergy = models.TextField()
    insurance = models.CharField(max_length=10, choices=INSURANCE_CHOICES, blank=True)

    def __str__(self):
        return '{0}'.format(self.first_name)


class PatientVisit(models.Model):
    # (This is the basic information about a patient's visit)

    VISIT_CHOICES = (
        ('in patient', 'in patient'),
        ('out patient', 'out patient'),
    )
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    doctor = models.ForeignKey(User, on_delete=models.CASCADE)
    visit = models.CharField(max_length=15, blank=False, null=False, choices=VISIT_CHOICES)
    diagnosis = models.TextField()
    current_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return '{0} {1}'.format(self.patient.first_name, self.patient.last_name)


class Laboratory(models.Model):
    # (adds laboratories in the system)
    name = models.CharField(max_length=25, blank=False, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Laboratories'


class Test(models.Model):
    # (Tests models)
    SPECIMEN_CHOICES = (
        ('SPECIMEN 1', 'SPECIMEN 1'),
        ('SPECIMEN 2', 'SPECIMEN 2'),
        ('SPECIMEN 3', 'SPECIMEN 3'),
    )
    patient_visit = models.ForeignKey(PatientVisit, on_delete=models.CASCADE)
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE)
    name = models.CharField(max_length=40)
    specimen = models.CharField(max_length=10, choices=SPECIMEN_CHOICES)
    result_expected = models.CharField(max_length=20, null=False, blank=False)
    date_rquested = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name
