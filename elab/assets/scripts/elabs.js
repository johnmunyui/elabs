$('a[data-elabs="get-patients"]').on('click', function(ev){
	ev.preventDefault();
	dpUI.loading.start("Loading ...");
	var contentsection = $('#content-section');	
		$.ajax({
			type: 'GET',
			url: 'http://127.0.0.1:8000/patients/',
			success: function(data){
        contentsection.hide()
				dpUI.loading.stop();
				contentsection.html(data);
        contentsection.fadeIn('6000');
			}
		});
	});


var allergy_section = $('div[data-elabs="allergy-section"]');
allergy_section.hide()

$('select[data-elabs="patient-select"]').on('change', function(ev){
  dpUI.loading.start("Loading ...");
  $.ajax({
      type: 'GET',
      url: 'http://127.0.0.1:8000/getallergy/',
      data: {'patient': ev.target.value },
      success: function(result){
        var paragraph = allergy_section.find('p');
        allergy_section.hide();
        dpUI.loading.stop();
        paragraph.html(result);
        allergy_section.fadeIn('6000');
      }
    });
});

$('a[data-elabs="get-served-patients"]').on('click', function(ev){
  ev.preventDefault();
  dpUI.loading.start("Loading ...");
  var contentsection = $('#content-section'); 
  $.ajax({
    type: 'GET',
    url: 'http://127.0.0.1:8000/patientsserved/',
    success: function(data){
      contentsection.hide()
      dpUI.loading.stop();
      contentsection.html(data);
      contentsection.fadeIn('6000');
    }
  });
});

$('a[data-elabs="get-requested-tests"]').on('click', function(ev){
  ev.preventDefault();
  dpUI.loading.start("Loading ...");
  var contentsection = $('#content-section'); 
  $.ajax({
    type: 'GET',
    url: 'http://127.0.0.1:8000/requestedtests/',
    success: function(data){
      contentsection.hide()
      dpUI.loading.stop();
      contentsection.html(data);
      contentsection.fadeIn('6000');
    }
  });
});