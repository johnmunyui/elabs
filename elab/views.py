# (import instaces that provide specific functionality)
from django.shortcuts import render  # (rendering a template)
from django.contrib.auth import authenticate, login, logout  # (handles authentication)
from django.http import HttpResponseRedirect, JsonResponse  # (response returning)
from django.views.generic import View, ListView  # (class based views)
from django.core.urlresolvers import reverse  # ( redirects to a url defined by name in the urls.py file)
from .models import *  # (imports all models defined in the models.py)
from .forms import *  # (import all forms)
import time
# Create your views here.


def actions(request):
    # renders the actions template
    return render(request, 'actions.html', {'doctor': request.user.username})


def signout(request):
    # handles the log out functionality
    logout(request)
    return HttpResponseRedirect('/')


def notifications(request):
    return render(request, 'notifications.html', {'doctor': request.user.username})


def get_allergy(request):
    # returns a json response of the chosen patient's allergy
    time.sleep(2)
    patient_id = request.GET['patient']
    patient = Patient.objects.get(id=patient_id)
    allergy = patient.allergy
    return JsonResponse(allergy, safe=False)


class Login(View):
    # this view handles authentication
    def get(self, request, *args, **kwargs):
        return render(request, 'login.html')

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('actions'))
        else:
            return render(request, 'login.html', {'authentication': 'failed'})


class PatientCreate(View):
    # adding a patient. Can be done by any doctor
    def get(self, request, *args, **kwargs):
        context = {'form': PatientForm()}
        return render(request, 'patient_create_form.html', context)

    def post(self, request, *args, **kwargs):
        form = PatientForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            return HttpResponseRedirect(reverse('actions'))
        else:
            context = {'form': form}
            return render(request, 'patient_create_form.html', context)


class PatientList(ListView):
    # returns the patients the logged in doctor has added
    template_name = 'patient_list.html'
    context_object_name = 'patients'

    def get_queryset(self):
        time.sleep(2)
        return Patient.objects.filter(added_by=self.request.user)


class PatientVisits(View):
    # Records a patient visit's details including diagnosis
    def get(self, request, *args, **kwargs):
        context = {'form': PatientVisitForm()}
        patients = Patient.objects.filter(added_by=request.user).only('id', 'first_name', 'last_name')
        context['patients'] = patients
        return render(request, 'patient_visit.html', context)

    def post(self, request, *args, **kwargs):
        form = PatientVisitForm(request.POST)
        if form.is_valid():
            patient = Patient.objects.get(id=request.POST['patient'])
            form.save(request.user, patient)
            return HttpResponseRedirect(reverse('actions'))
        else:
            context = {'form': form}
            patients = Patient.objects.filter(added_by=request.user).only('id', 'first_name', 'last_name')
            context['patients'] = patients
            return render(request, 'patient_visit.html', context)


class PatientVisitList(ListView):
    # shows the patients the logged in doctor has served
    template_name = 'patient_visit_list.html'
    context_object_name = 'visits'

    def get_queryset(self):
        time.sleep(2)
        return PatientVisit.objects.select_related('doctor', 'patient').filter(doctor=self.request.user)


class RequestTest(View):
    # this is where a doctor requests one or more tests
    def get(self, request, *args, **kwargs):
        context = {'form': TestForm()}
        laboratories = Laboratory.objects.all()
        context['laboratories'] = laboratories
        return render(request, 'test_request.html', context)

    def post(self, request, *args, **kwargs):
        form = TestForm(request.POST)
        if form.is_valid():
            laboratory = Laboratory.objects.get(id=request.POST['laboratory'])
            tests = []
            if 'test1' in request.POST:
                tests.append(request.POST['test1'])
            if 'test2' in request.POST:
                tests.append(request.POST['test2'])
            if 'test3' in request.POST:
                tests.append(request.POST['test3'])

            tests = ', '.join(tests)
            form.save(laboratory, tests)
            return HttpResponseRedirect(reverse('actions'))
        else:
            context = {'form': form}
            laboratories = Laboratory.objects.all()
            context['laboratories'] = laboratories
            return render(request, 'test_request.html', context)


class TestsRequestedList(ListView):
    # Displays the tests requested by a doctor
    template_name = 'tests_requested.html'
    context_object_name = 'tests'

    def get_queryset(self):
        time.sleep(2)
        return Test.objects.select_related('laboratory', 'patient_visit').filter(patient_visit__doctor=self.request.user)


class EditTest(View):
    def get(self, request, id, *args, **kwargs):
        test = Test.objects.get(id=id)
        form = TestUpdateForm(instance=test)
        laboratories = Laboratory.objects.all()
        context = {}
        context['form'] = form
        context['laboratories'] = laboratories
        context['id'] = id
        return render(request, 'edit_test.html', context)

    def post(self, request, *args, **kwargs):
        form = TestUpdateForm(request.POST)
        id = request.POST['test_id']
        test = Test.objects.get(id=id)
        if form.is_valid():
            laboratory = Laboratory.objects.get(id=request.POST['laboratory'])
            tests = []
            if 'test1' in request.POST:
                tests.append(request.POST['test1'])
            if 'test2' in request.POST:
                tests.append(request.POST['test2'])
            if 'test3' in request.POST:
                tests.append(request.POST['test3'])

            specimen = request.POST['specimen']
            result_expected = request.POST['result_expected']
            tests = ', '.join(tests)

            test.laboratory = laboratory
            test.name = tests
            test.specimen = specimen
            test.result_expected = result_expected
            test.save()
            return HttpResponseRedirect(reverse('actions'))
        else:
            context = {'form': form}
            laboratories = Laboratory.objects.all()
            context['laboratories'] = laboratories
            context['id'] = id
            return render(request, 'edit_test.html', context)


class DeleteTest(View):
    def get(self, request, id, *args, **kwargs):
        return render(request, 'test_delete.html', {'id': id})

    def post(self, request, *args, **kwargs):
        id = request.POST['test_id']
        test = Test.objects.get(id=id)
        test.delete()
        return HttpResponseRedirect(reverse('actions'))