from django import forms
from .models import *


class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        fields = ['first_name', 'last_name', 'middle_name', 'date_of_birth',
                  'email', 'mobile_number', 'id_number', 'passport_number',
                  'allergy', 'insurance']

    def save(self, user):
        patient = super(PatientForm, self).save(commit=False)
        patient.added_by = user
        patient.save()
        return True


class PatientVisitForm(forms.ModelForm):
    class Meta:
        model = PatientVisit
        fields = ['visit', 'diagnosis']

    def save(self, user, sickpatient):
        visit = super(PatientVisitForm, self).save(commit=False)
        visit.patient = sickpatient
        visit.doctor = user
        visit.save()
        return True


class TestForm(forms.ModelForm):
    class Meta:
        model = Test
        fields = ['patient_visit', 'specimen', 'result_expected']

    def save(self, laboratory, tests):
        test = super(TestForm, self).save(commit=False)
        test.laboratory = laboratory
        test.name = tests
        test.save()
        return True

class TestUpdateForm(forms.ModelForm):
    class Meta:
        model = Test
        fields = ['specimen', 'result_expected']
        