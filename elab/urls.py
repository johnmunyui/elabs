# (elabs urls)

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from elab.views import *

urlpatterns = [
    url(r'^$', Login.as_view(), name='login'),
    url(r'^actions/$', login_required(actions), name='actions'),
    url(r'^logout/$', login_required(signout), name='signout'),
    url(r'^getallergy/$', get_allergy),
    url(r'^addpatient/$', login_required(PatientCreate.as_view()), name='create-patient'),
    url(r'^patients/$', PatientList.as_view(), name='patient-list'),
    url(r'^patientvisit/$', login_required(PatientVisits.as_view()), name='patient-visit'),
    url(r'^patientsserved/$', PatientVisitList.as_view(), name='patients-served'),
    url(r'^testrequest/$', login_required(RequestTest.as_view()), name='test-request'),
    url(r'^requestedtests/$', TestsRequestedList.as_view(), name='tests-requested'),
    url(r'^edittest/(?P<id>\d+)/$', login_required(EditTest.as_view()), name='test-edit'),
    url(r'^deletetest/(?P<id>\d+)/$', login_required(DeleteTest.as_view()), name='delete-test'),
    url(r'^notifications/$', notifications, name='notifications')
]
